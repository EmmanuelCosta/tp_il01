#!/usr/bin/python
import urllib,os,sys
import nltk
import codecs
from nltk.tokenize import *

def tokenize_that_exo1_1(t):
	print regexp_tokenize(t,pattern=r'[a-zA-Z]+|[0-9]|[,.?;]')
	
def tokenize_that_exo1_2(t):
	print regexp_tokenize(t,pattern=r'[a-zA-Z]+|[\d.]+|[,.?;]')

#todo	 
def tokenize_that_exo1_3(t):
	print regexp_tokenize(t,pattern=r'\d+[/\|\-. ]\d+[/|\-\. ]\d+') 

if __name__ == "__main__":
	if len(sys.argv) < 1:
		print "not enough argument : missing filename to parse"

	filename= sys.argv[1]
	f = codecs.open(filename, 'r3', encoding='utf-8')

	for s in f :		
		tokenize_that_exo1_1(s)
	