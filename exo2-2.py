#!/usr/bin/python

import urllib,os,sys
import nltk
import codecs
from nltk.tokenize import *
import pickle


def search(query,indexFile,disjonction=True):
	dico={}
	f=open(indexFile,"r")
	dico = pickle.load(f)
	resultList=[]
	print disjonction
	
	for word in query.split():
			try:

				resultList=resultList+dico[word]
			except KeyError:
				if disjonction :
					print "no match for "+word
				else :
					return []

	return resultList

if __name__=="__main__" :
	if len(sys.argv) < 1:
		print "not enough argument : missing filename to parse"

	
	indexFile=sys.argv[1]
	
	while True :
		query =raw_input("enter a query :")
		print search(query,indexFile)

