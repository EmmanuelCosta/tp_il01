#!/usr/bin/python
import urllib,os,sys
import nltk
import codecs
from nltk.tokenize import *
import pickle



#do that just for debug purpose
def writeResultInFile(dico):	
	f=codecs.open("result.txt","w",encoding="UTF-8")
	for k,v in dico.iteritems() :
		rendu=k+": "+"[ "+"".join(v)+" ]"
		f.write(rendu)

def saveResult(path):
	indexFile=codecs.open(path,"w",encoding="UTF-8")
	pickle.dump(dico,indexFile)
	indexFile.close()

if __name__ == "__main__" :
	if len(sys.argv) < 1:
		print "not enough argument : missing filename to parse"

	
	collectionList=sys.argv[1]
	#retrieve file from collection list file
	f=codecs.open(collectionList,"r",encoding="UTF-8")
	x= map(lambda t : ("reuter-collection-tp/"+t).strip(),f.readlines())
	f.close()

	dico={}
	
	for path in x:
		filename=codecs.open(path,"r",encoding="UTF-8")

		for text in filename:
			wordList= nltk.regexp_tokenize(text,"\w+")
			for word in wordList :
				if word in dico :
					if path not in dico[word] : dico[word].append(path)
				else:
					dico[word]=[path]


	saveResult("result")

	


	

	

	
